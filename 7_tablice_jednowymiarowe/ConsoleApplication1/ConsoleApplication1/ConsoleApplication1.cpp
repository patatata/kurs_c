// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
//Tablica - to ciag zmiennych (obiektow) tego samego typu. Zajmuja one ciagly obszar w pamieci.
//Na tej lekcji omawiana jest tablica statyczna. Tablica statyczna to taka ktorej rozmiar nalezy
//podac jeszcze przed uruchomieniem, poniewaz kompilator musi wiedziec ile ma zarezerwowac miejsca 
//w pamieci dla tej tablicy. 

#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <time.h>

#define N 5

using namespace std;

int main()
{

	const int R = 20; //indeksowanie od 0 do R-1
	int i;
	int sum = 0;
	//inicjalizacja tablicy 
	//int tab1[R] = {3, 4, 2, 6, 5, 5, 6, 6, 3, 6}; //inicjalizacja zbiorcza, pozostale elementy inicjalizowane jako 0
	//tab[0] = 3; //inicjalizacja pojedynczego elementu
	int tab1[R]; //sama deklaracja bez inicjalizacji, skutek: smieci w tablicy
	int tab2[N];
	int tab3[10]= {}; //nowosc w C++11 inicjalizacja tablicy zerami
	int tab4[3]{ 1,2,3 }; //nowosc w C++11 pominiecie znaku '='
	//int tab[] = {1,2,3,4,5,6,7}; //jezeli wystepuje inicjalizacja zbiorcza nie jest potrzebne okreslanie rozmiaru tablicy

	//Przyklady
	//Czyszczenie tablicy za pomoca petli for 
	//for (i = 0; i < R; i++) tab1[i] = 0;
	//Wczytywanie danych do tablicy 
	//for (i = 0; i < R; i++) cin>>tab1[i];
	//Sumowanie wartosci elementow tablicy
	//for (i = 0; i < R; i++) sum += tab1[i];

	//Odwracanie szeregu liczbowego
	//Najpierw pobranie liczb
	for(i=0; i<10; i++)
	{
		cout << tab3[i] << endl ;
	}
	
	for (i = 0; i < N; i++)
	{
		cout << "Podaj liczbe nr " << i << "\n";
		cin >> tab2[i];
	}
	cout << "Twoj ciag liczb" << endl;
	for (i = 0; i <N; i++) cout << tab2[i] << " ";
	cout << endl;
	cout << "Liczby wspak" << endl;
	for (i = N - 1; i >= 0; i--) cout << tab2[i] << " ";
	
	int v[] = {1, 4, 5, 2, 79, 43, 2};
	for(auto x : v) //dla kazdego x w v
		cout << x << endl;
	//uproszczona wersja do przegladania tablica
	for()
	//srand(time(NULL)); //ustawia punkt startowy do losowania pseudolosowych liczb.
	////Ustawienie wartosci innej niz 1 powoduje, ze za kazdym razem losowana jest inna liczba

	//int i;
	//for (i = 0; i < R; i++)
	//{
	//	tab[i] = rand() % 10;
	//}

	////szukanie maksymalnej wartosci w tablicy
	//int max = tab[0];
	//for (i = 1; i < R; i++)
	//{
	//	if (tab[i] > max)
	//	{
	//		max = tab[i];
	//	}
	//}
	////cout - sia�t, console out
	//printf("Max = %d\n", max);

	////szukanie minimalnej wartosci w tablicy
	//int min = tab[0];
	//for (i = 1; i < R; i++)
	//{
	//	if (tab[i] <min)
	//	{
	//		min = tab[i];
	//	}
	//}
	//printf("Min = %d\n", min);

	////srednia
	//float suma = 0.0;
	//float srednia = 0.0;
	//for (i = 0; i < R; i++)
	//{
	//	suma += tab[i];
	//}
	//srednia = suma / R;


	getchar();
	getchar();
    return 0;
}

/*

zad 1.
znajdz minimum i maksimum tabd

zad 2.
zaimplementuj tablice stayczna dla elementow typu int o rozmiarze R, a nastepnie zlicz ile w tej tablicy znajduje sie liczb wiekszych od 5

zad 3.
zaimplementuj tablice statyczna elementow typu int o rozmiarze R, a nastepnie zlicz ile razy wystapila w niej liczba, ktora podaje uzytkownik

zad 4.
zaimplementuj tablice statyczna elementow typu float o rozmiarze R, niech wypelni ja uzytkownik a nastepnie niech  program znajduje
iloczyn elementu najwiekszego w tablicy i najmniejszego

Skroty klawiszowe 
Ctrl + K a nastepnie Ctrl + C - komentarz z zaznaczonej czesci kodu, Ctrl + U - usuniecie komentarza zaznaczonej czesci kodu

*/