#include "stdafx.h"
#include "library.h"
#include <conio.h>
#include <iostream>
#include <stdlib.h>

using namespace std;
extern LibraryDB book;
extern fstream file;
//extern fstream fileout;
unsigned int cur_line;
void addBook()
{

	//wprowadzic opcje wyjscia z dodawania ksiazki 
	int category;
	cout << "*******************************************************" << endl;
	cout << "Specify category (od 1 do 4): " << endl;
	cout << "\nComputer Science \nEpic poetry \nNovel \nFantasy \nPsychology \nBack to menu" << endl;
	getline(cin,book.category);
	cout << "Title of the book: " << endl;
	getline(cin, book.bookName); //cin<<title pobra�by tylko jeden wyraz
	cout << "Author name and surname: " << endl;
	getline(cin, book.authorNameSurname);
	book.rented = 0;
	cout << "Enter year of publication: " << endl;
	cin >> book.BookDate.year;
	cout << "Enter month of publication: " << endl;
	cin >> book.BookDate.month;
	cout << "Enter day of publication: " << endl;
	cin >> book.BookDate.day;
	file.open("library.txt", ios::out | ios::app); //drugi argument pozwala na dopisywanie informacji do pliku
	file << "Category: " << book.category << "\nTitle: " << book.bookName << "\nAuthor: " << book.authorNameSurname <<
		"\nDate: "<< book.BookDate.day << "-" << book.BookDate.month << "-" << book.BookDate.year << "\nNiewypozyczona" << "\n \n" << endl;

	file.close();


}

void deleteBooks()
{
	file.open("library.txt", ios::out | ios::trunc);
	file.close();
}

void showBooks()
{
	int line_number = 1;
	file.open("library.txt", ios::in | ios::out | ios::app); //pokieruj strumien danych z pliku do programu
	if (file.is_open() == 0)
	{
		cout << "The file can not be opened" << endl;
		exit(0); //exit jest zadeklarowany w bibliotece stdlib.h
	}
	else
	{
		string line;
		while(getline(file, line)) //getline pobiera kolejne linie, zwraca fa�sz gdy nie ma juz linii, pierwszy argument to zawsze skad chcemy dane, dokad
		{
			cout << line << endl;
		}
	
	}

	file.close();
}

bool searchBook(string title)
{
	bool found = 0;
	int display_book = 0;
	file.open("library.txt", ios::in);
	//przeszukiwanie pliku w poszukiwaniu tytulu i jezeli sie znajdzie wypisanie tego tytulu i 3 pozostalych linii pod nim
	string line;
	cur_line = 0;
	while (getline(file, line))
	{
		cur_line++;
		if (line.find(title, 0) != string::npos) //przyjmuje tekst i pozycje od ktorego ma szukac, jezeli linie sa rozne to ta funkcja zwraca string::npos
		{
			cout << "found in line: " << cur_line << endl;
			found = 1;
			display_book = 3;
 		}
		if (display_book > 0)
		{
			cout << line << endl;
			display_book--;
		}
	}

	file.close();

	if (found) return 1;
	else return 0;
	
}

//ustawienie pozycji dwie linie pod tytulem na poczatku trzeciej linii 
void changeStatus(string title, bool state)
{
	string line;
	file.open("library.txt", ios::in | ios::out | ios::app); //dopisywanie do pliku
	//file.seekg(0, file.beg);
	if (file.is_open() == false)
	{
		cout << "Error opening file library.txt" << endl;
	}

	string strReplace = "Niewypozyczona";
	string strNew = "Wypozyczona";

	while (getline(file, line))
	{
		auto length = file.tellg();
		if (line == strReplace)
		{
			file.seekg(length);	
			file << strNew;
		}

	}


//	fileout.open("library_out.txt", ios::in);
//	if (fileout.is_open() == false)
//	{
//		cout << "Error opening file library_out.txt" << endl;
//	}



//	string strTemp;
//	while (getline(file, strTemp))
//	{
//		if (strTemp == strReplace)
//		{
//			strTemp = strNew;
//		}
//		strTemp += "\n";
//		fileout << strTemp;
//	}

	file.close();
//	fileout.close();
}


/*
//written by Patrycja Paduch
Funkcja wpisywania hasla przy uzyciu conio.h
int getch(void) - pozwala na odczytywanie pojedynczego znaku, zwraca znak w postaci numeru w ascii
wypisywanie tego co wpisalismy za pomoca cout << getch()
*/
string checkPassw()
{
	 string password = "";
	const char ENTER = 13;
	const char BACKSPACE = 8;
	char ch = 0;
	while ((ch = _getch()) != ENTER)
	{
		if (ch == BACKSPACE)
		{
			if (password == "")
			{

			}
			else
			{
				cout << "\b \b"; //jezeli mamy 3 gwiazdki ***, \b cofa kursor na druga gwiazdke, " " - usuwa trzec
				// po  czym \b znowu cofa kursor
				password.erase (password.length() -1, 1);		
			}
		}
		else
		{
			password += ch;
			cout << "*";
		}
	}
	cout << endl;
	return password;
}

