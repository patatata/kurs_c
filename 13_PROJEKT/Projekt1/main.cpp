// Projekt1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h" 
#include "library.h"

using namespace std;

/*
Napisac program ktory pozwala na wprowadzanie i przechowywanie danych o ksiazkach w bibliotece.
Dane moga byc przechowywane w strukturze

Struktura Biblioteka zawierałaby 
- id (unikalna liczba calkowita przypisywana automatycznie przez program najlepiej od 1)
- nazwa ksiazki
- imie i nazwisko autora
- kategoria 
- czy wypozyczona 
- data wypozyczenia
- imie wypozyczajacego
- nazwisko wypozyczajacego


Menu programu
- Dodawanie nowej ksiazki
- Usuwanie ksiazki
- Zmiana statusu ksiazki wypozyczona/niewypozyczona 
- Dodawanie danych wypozyczajacego ksiazke
- Wyszukiwanie ksiazki po tytule
- Edycja danych

1. Stworzenie szkieletu, podzial na pliki.
2. Stworzenie interfejsu dla uzytkownika, w petli, zabezpieczenia przed bledami, wybor bez entera
3. stworzenie struktury

*/

//Nalezy zabezpieczyc sie przed niepoprawnie wpisana wartoscia. Mozna to zrobic 
//- uzywajac znaku char jako wybor wtedy choose=getch();
//- uzywajac znaku int jako wybor if(!(cin >> choose)) cerr<<"To nie jest liczba";
//  jezeli uzytkownik wpisze np. 2we to we zostanie uciete, w przeciwnym przypadku program
//  moze zachowac sie w sposob niezdefiniowany
LibraryDB book;

fstream file("library.txt");
//fstream fileout("library_out.txt");
char chh;

int main()
{
	int x = 1;
	int choose = 0;
	const string password = "library123456";
	string checkpasword;
	string title;
	bool state = 0;
	char ch;
	cout << "***** Application protected *****" << endl;
	cout << "Print password: ";
	do
	{
		checkpasword = checkPassw();
		if (password == checkpasword)
		{
			cout << "Logged in successfully. Press any key to continue..." << endl;
		}
		else
		{
			cout << "Wrong password. Try again..." << endl;
			checkpasword = "";
		}
	} while (password != checkpasword);

	

	while (x)
	{
		cout << "LIBRARY. MENU: " << endl;
		cout << "1. Add new book" << endl;
		cout << "2. Delete book" << endl;
		cout << "3. Change status of a book" << endl;
		cout << "4. Search book by the title" << endl;
		cout << "5. Show books" << endl;
		cout << "6. Exit" << endl;
		cout << endl;
		
		cin >> choose; //cin nie pobiera entera przez co ten enter zostanie odczytanie w nastepnej instrukcji pobierajacej
		//aby tego uniknac uzywamy getchar()
		while (cin.fail()) //metoda nalezaca do strumienia cin, jezeli wczytanie sie nie powiedzie zwraca 1
		{
			cin.clear(); //metoda czysci flage bledu
			cin.ignore(INT_MAX, '\n'); //pozwala na ignorowanie bledow az do napotkania znaku przejscia do nowej lini
			cout << "Nie podales liczby. Sprobuj jeszcze raz: " << endl;
			cin >> choose;
		}
		getchar(); //pobiera jeden znak czyli enter '\n'
		
		switch (choose) //zmienna sterujaca switchem moze byc int lub char, ale nie float ani string
		{
		case 1:
			addBook();

			break;
		case 2:
			deleteBooks();
			break;
		case 3:
			
			cout << "Press 1 to rent a book, press 0 to return a book" << endl;
			cin >> state;
			cin.ignore();
			cout << "Give title of the book" << endl;
			getline(cin, title);
			changeStatus(title, state);
			getchar();
			break;
		case 4:
			cout << "Give title of the book" << endl;
			getline(cin, title);
			searchBook(title);
			getchar();
			break;
		case 5:
			showBooks();
			getchar();
			break;
		case 6:

			break;
		case 7:
			//exit(0);
			x = 0;
			break;
		default:
			cout << "Niepoprawny wybor. Sprobuj ponownie." << endl;
			break;
		}
		
		system("cls"); //czyszczenie okna
	}

	

	getchar();
    return 0;
}

