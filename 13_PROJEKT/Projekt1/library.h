#pragma once
#include "stdafx.h"
#include <string>
#include <iostream>
#include <conio.h>
#include <fstream> //biblioteka udostepniajaca funkcje do zapisu do pliku

using namespace std;

struct Date
{
	int day;
	int year;
	int month;
};

struct LibraryDB
{
	int id;
	string bookName;
	string authorNameSurname;
	string category;
	bool   rented;
	Date BookDate;
	string borrowerName;
	string borrowerSurname;
};

void addBook();
void deleteBooks();
string checkPassw();
void showBooks();
bool searchBook(string title);
void changeStatus(string title, bool state);