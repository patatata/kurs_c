// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	//operator sizeof dla tablic, przydatny gdy przewidujemy zmiane rozmiaru tablicy 
	const int R = 10;
	int size, i;
	int tab[R];
	size = sizeof(tab);
	cout << size << endl;
	for (i = 0; i < sizeof(tab) / sizeof(tab[0]); i++)
	{
		tab[i] = 0;
	}

	getchar();
    return 0;
}

/*,
zad1
tablica statyczna o rozmiarze R, dane losowane
a) zliczamy wystapienie elementow parzystych i podzielnych przez 8
b) sumujemy elementy, ktore sa mniejsze od liczby podanej przez uzytkownika
c) elementy ktore sa wieksze od 15 zamieniamy na liczbe przeciwna czyli jak mamy element 16 to zamieniamy go na -16 a jak 10 to nic mu nie robimy
*/

/* //***************************************************************************************************
srand(time(NULL));
const int R = 10;
int tab[R];
int i;
int licznik = 0;
int licznik1 = 0;

//losowanie liczb do tablicy
for (i = 0; i < R; i++)
{
tab[i] = rand() % 100;
}
printf("Wylosowano nastepujace liczby \n");
for (i = 0; i < R; i++)
{
printf("%d \n", tab[i]);
}

printf("Ilosc liczb parzystych i podzielnych przez 8 jest rowna % ");
for (i = 0; i < R; i++)
{
if (tab[i] % 2 == 0 && tab[i] % 8 == 0)
{
licznik++;
}
}

/* //************************************c************************************************************************
srand(time(NULL));
const int R = 10;
int tab[R];
int i;
int x;
int suma = 0;
for (i = 0; i < R; i++)
{
tab[i] = rand() % 100;
}
printf("Wylosowano nastepujace liczby \n");
for (i = 0; i < R; i++)
{
printf("%d \n", tab[i]);
}
printf("Uzytkowniku podaj liczby \n");
scanf_s("%d", &x);
getchar();

for (i = 0; i < R; i++)
{
if (tab[i] < x)
{
suma += tab[i];
}

}
printf("Suma jest rowna %d", suma);
*/ //*******************************************************d*******************************************************
/*
srand(time(NULL));
const int R = 10;
int tab[R];
int i;
int x;
int suma = 0;
for (i = 0; i < R; i++)
{
	tab[i] = rand() % 100;
}
printf("Wylosowano nastepujace liczby \n");
for (i = 0; i < R; i++)
{
	printf("%d \n", tab[i]);
}
printf("Liczby wylosowane oraz przeciwne wiekszych od 15 to: \n \n ");
for (i = 0; i < R; i++)
{
	if (tab[i]>15)
	{
		tab[i] = -1 * tab[i];
	}
	printf("%d \n", tab[i]);
}
getchar();

*/