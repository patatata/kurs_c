// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std; 


//Przekazywanie tablicy do funkcji - 2 opcje:

// Przekazywanie zmiennych do funkcji przez wartosc - kiedy przekazujemy do funkcji jakas zmiennna zostaje utworzona 
// w pamieci jej kopia. Wszelkie operacje w srodku funkcji wykonuja sie na tej kopii. Jedyna opcja przekazania
// zmiennej poza obszar funkcji jest uzycie opcji return, jednak w ten sposob mozna zwrocic tylko jedna funkcje 

// Przekazywanie zmiennych do funkcji przez wskaznik - do funkcji przekazywane sa adresy zmiennych, zamiast tworzenia
// kopii tych zmiennych. W przypadku modyfikacji argumentow w srodku funkcji, zmiany beda widoczne na zewnatrz.

// Przekazywanie tablicy do funkcji - tablice przekazuje sie do funkcji poprzez podanie adresu poczatku tej tablicy.
// Wazna uwaga: NAZWA TABLICY JEST ADRESEM W PAMIECI JEJ PIERWSZEGO ELEMENTU. W ten sposob
// zmiany w tablicy wykonane w srodku funkcji sa widoczne rowniez poza obszarem tej funkcji. 
// deklaracja: int funkcja(int tab[], rozmiar) - podajemy tablice bez rozmiaru, rozmiar podajemy z palca oddzielnie
// wywolanie funkcja(tab) - podajemy nazwe tablicy
//nie trzeba podawac rozmiaru tablicy w deklaracji oraz definicji 

float srednia(int n, int tab[]);
void objetosc(int n, int tab[]);
void fibonacci();
void generuj(int n, int tab[]);

int main()
{
	srand(time(NULL));
	const int R = 10;
	int tab[] = {1, 4, 5, 2};
	int tab1[5];
	int objj[4] = {};
	int size_tab;
	size_tab = sizeof(tab) / sizeof(tab[0]);
	float srednia1;
//	srednia1 = srednia(size_tab, tab);
	generuj(5, tab1);
//	cout << "Srednia liczb w tablicy <<" <<srednia1 << endl;
//	fibonacci();

	getchar();
    return 0;
}

//Zadanie 1. Napisz funkcje ktora otrzymuje 2 argumenty: dodatnia liczbe calkowita n oraz n-elementowa tablice tab
//o elementach typu int i zwraca jako wartosc srednia arytmetyczna elementow tablicy tab
float srednia(int n, int tab[]) //funkcja o tablicy wie tylko jaki jest adres jej pierwszego elementu oraz jaki jest typ tej tablicy
{
	int i;
	int sum = 0;

	for (i = 0; i < n; i++)
	{
		sum += tab[i];
	}	

	return (sum /n);
	return sum;
}

//Zadanie 2.Napisz funkcje ktora wygeneruje 40 elementow ciagu fibonacciego 0, 1, 1, 2
void fibonacci()
{
	int tab[40] = {};
	cout << "Ciag fibonacciego \n";
	tab[1] = 1;	
	for (int i=2; i < 40; i++)
	{
		tab[i] = tab[i-1] + tab[i - 2];
	}
	for (int i = 0; i < 40; i++)
	{
		cout << tab[i] << " ";
	}

}

//Zadanie 3. Zalozmy, ze masz 5 roznych szescianow. Ich krawedzie wynosza 2, 5, 6, 7, 8. Umiesc dane o krawedziach 
// w odpowiedniej tablicy a nastepnie napisz funkcje ktora podmieni wartosci krawedzi na wartosci objetosci takich szescianow

void objetosc(int n, int tab[])
{
	int obj = 0;
	for (int i = 0; i < n; i++)
	{
		tab[i] = tab[i] * tab[i] * tab[i];
	}

}

//Zadanie 4.  Napisz funkcje ktora bedzie generowala pseudolosowe liczby i zapisywala je do tablicy. 
//Nastepnie napisz funkcje ktora odgadnie wygenerowane haslo, zlicz ile razy generowano haslo aby je odgadnac

void generuj(int n, int tab[])
{
	int i;
	for (i = 0; i < n; i++)
	{
		tab[i] = rand() % 9 + 1;
	}

	for (i = 0; i < n; i++)
	{
		cout << tab[i] << " ";
	}	
}


void odgadnij(int n, int tab[])
{
	int i;
	int pass = 0;
	while (pass == 0)
	{
		for (i = 0; i < n; i++)
		{

			tab[i] = rand() % 9 + 1;
		}
	}	
}

void odgadnij_lepiej(int n, int tab1[], int tab2[])
{
	
	for(int i)
}