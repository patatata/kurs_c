// 10_tablice_znakowe.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

//Do przechowywania lancuchow znakowych sluza:
//tablice znakowe 
//funkcje klasy string

// tablica znak�w - (w stylu C). Ostatnim znakiem w takiej tablicy jest zawsze znak \0

int main()
{
	char napis1[] = { 'A', 'l', 'a', ' ', 'm','a', ' ', 'k','o','t','a', '\0' }; //bez nulla na koncu nie bylby to c-string.
	char napis_zly[] = { 'A','l','a' }; //wiele funkcji np. cout dziala na zasadzie wypisywania az do napotkania null
	//jezeli nulla nie ma dane sa wypisywane wraz ze smieciami w pamieci az do napotkania nulla
	cout << napis1 << endl;
	char ksiazka[] = "W pustyni i w puszczy"; //kompilator dzieki uzyciu " " rozpoznaje wyrazenie jako c-string i dopisuje null 
	char zwierze[4] = "kot"; //liczymy rowniez null
	char imie[6] = "Ala"; //pozostale elementy zapelniane zerami
	cout << ksiazka << endl;
	string tekst = "Ala ma kota"; //przy typie string nie trzeba podawac dlugosci tekstu, mozna uzywac z cout i cin, klasa
	cout << tekst << "\nMa on dlugosc: " << tekst.length() << endl; //nalezy uwazac przy funkcji cin na spacje

	
	//znajdz przynajmniej 4 metody klasy string


	getchar();
	getchar();
	getchar();
    return 0;
}


//funkcja kopiujaca zawartosc jednej tablicy do drugiej
void copyTab(char dest[], char source[])
{
	int i = 0;
	for (i = 0; ; i++) //nie wiadomo ile elementow ma przekazywana tablica 
	{
		dest[i] = source[i];
		if (dest[i] == 0) break; //null ma wartosc 0 w ascii
	}
}


/*
Ile elementow ma tak zdefiniowana tablica
char tab[] = "1234"; 
Czy ponizsza tablica jest poprawnie zadeklarowana:
char tab[4] = {"0,1,2"}; 
*/

