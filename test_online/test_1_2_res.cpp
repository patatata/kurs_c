//1. Jezeli piszac int nie zaznaczyles czy chodzi Ci o wersje signed czy usnigned,
//ktory wariant wybierze kompilator?

signed																				//1/1


//2. Napisz najlepszy mozliwy typ (pamietaj o oszczedzaniu pamieci)
//a. stan logiczny na pinie mikrokontrolera - bool		OK								
//b. liczba: 32 768    short ma zakres od -32 768 do 32 767 wi��c unsigned short OK
//c. napis: "To jest trudniejszy podpunkt" w sumie nie wiem... wi�c strzelam- int NOK
//d. dzisiejsza temperatura char OK (ale mogloby byc unsigned char)
//e. 14-cyfrowa liczba long long OK
//f. tablica znakow char OK															//5/6

//3. Ktore z trzech petli jakie poznales moga sprawic, ze zawarte w niej instrukcje
//nie zostana wcale wykonane. Napisz przyklad kodu (dla kazdej petli ktora podasz) z uyciem 
//takich petli, tak aby wgl program nie wszedl do srodku petli

     int i;
     for (i=1; i==5; ++i)
     {
         if (i=7)
         {
             cout <<"hehe"<<endl;
         }

     

     int a =10;
     while (a > 20)
     {
          cout<<"cos tam "<< a<<endl;
          a++ ;
     }



     int i=10;
     if(i>10)
     {
          cout <<"elo"<<endl;
     }
																					//2/2
	

//4. Zalozmy, ze wykonujesz pewne operacje w petli nieskoczonej while(1) { }. Co zastosowac
//jezeli w przypadku podania przez uzytkownika niepoprawnej danej przerwac ta petle nieskonczona
// i przejsc do kolejnej instrukcji wystepujace po tej petli

continue																			//0/2

//5. Petla switch jest szczegolnie uzyteczna gdy zalezy nam na szybkosci dzialania. Jest znacznie szybsza
// niz petla if-else. Co sie jednak stanie gdy zapomnisz w danym case'ie napisac break?

wtedy po wykonaniu case'a zamiast zako�czy� program powt�rzy case'a				     //0/1


//6. Jaka jest roznica pomiedzy break a continue, wytlumacz pamietajac o wszystkich aspektach.


break bezwarunkowo przerywa dzia�anie p�tli (wychodzi z tej p�tli). Je�li dalsze wykonywanie p�tli nie ma sensu, przerywamy jej dzia�anie w�a�nie t� instrukcj�.
continue, powoduje wznowienie dzia�ania p�tli i zaprzestanie wykonywania dalszych instrukcji dla danego powt�rzenia. Stosuje si� je w przypadku, 
gdy w danej iteracji p�tli nie ma konieczno�ci wykonania dalszych instrukcji p�tli.  // 1/1 


//7. Co wypisze program w przypadku a, a co w przypadku b.
//a														OK
    int i=0;
    while(i<10) {
    	cout << ++i << endl;
    }

wtedy program wypisze liczby : 1,2,3,4,5,6,7,8,9,10 											

//b
    int i=0;
    while(i<10) {
    	cout << i++ << endl;
    }

wtedy program wypisze liczby : 1,2,3,4,5,6,7,8,9     //NOK
//																						//1/2



//10/15