//1. Jezeli piszac int nie zaznaczyles czy chodzi Ci o wersje signed czy usnigned,
//ktory wariant wybierze kompilator?


//2. Napisz najlepszy mozliwy typ (pamietaj o oszczedzaniu pamieci)
//a. stan logiczny na pinie mikrokontrolera -
//b. liczba: 32 768
//c. napis: "To jest trudniejszy podpunkt" 
//d. dzisiejsza temperatura
//e. 14-cyfrowa liczba
//f. tablica znakow

//3. Ktore z trzech petli jakie poznales moga sprawic, ze zawarte w niej instrukcje
//nie zostana wcale wykonane. Napisz przyklad kodu (dla kazdej petli ktora podasz) z uyciem 
//takich petli, tak aby wgl program nie wszedl do srodku petli

//4. Zalozmy, ze wykonujesz pewne operacje w petli nieskoczonej while(1) { }. Co zastosowac
//jezeli w przypadku podania przez uzytkownika niepoprawnej danej przerwac ta petle nieskonczona
// i przejsc do kolejnej instrukcji wystepujace po tej petli

//5. Petla switch jest szczegolnie uzyteczna gdy zalezy nam na szybkosci dzialania. Jest znacznie szybsza
// niz petla if-else. Co sie jednak stanie gdy zapomnisz w danym case'ie napisac break?

//6. Jaka jest roznica pomiedzy break a continue, wytlumacz pamietajac o wszystkich aspektach.


//7. Co wypisze program w przypadku a, a co w przypadku b.
//a
    int i=0;
    while(i<10) {
    	cout << ++i << endl;
    }

//b
    int i=0;
    while(i<10) {
    	cout << i++ << endl;
    }


