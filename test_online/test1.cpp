//1. Dobierz najlepszy typ do zmiennej (lub stalej), pamiętaj o oszczędności pamięci:
//pierwsza litera imienia -
//Twoj wiek
//stan bitu
//Twoja waga
//Twoj rok urodzenia
//jak najlepsza dokladnosc liczby pi 
//temperatura na Syberii w zimie
//ilosc ludzi na swiecie
//enter


//2. Uszereguj typy rosnąco (przyklad zapisu: pszczola<pies<krowa<slon)
//podaj jak najwiecej typow ktore Ci przychodza na mysl


//3. Wyjasnij czym sie rozni operator postinkremementacji i preinkrementacji na przykladzie kodu 

//4. Napisz 2 opcje kodu na wypisywanie bez konca "Jeszcze tyle nauki"


//5. Wytlumacz Pani Zosi z warzywniaka jaka jest roznica pomiedzy break a continue

//6. Jezeli chcesz aby iteracja w petli wykonala sie chociaz raz jakiego typu petli uzyjesz?

//7. Napisz sprawdzanie czy podana liczba jest w przedziale <9; 14) lub jest podzielna przez 3. Jezeli nie to natychmiast przerywasz
//dzialanie petli

//8. Co wypisze konsola:
//a
int j=6;
while(++j <10)
	cout << j++ << endl;
//b
int x;
x = 8.25;

//9. Napisz petle for ktora wypisze 1, 2, 4, 8, 16, 32, 64. Uzyc odpowiedniego operatora inkrementacji
