// wskazniki_przekazywanie_funkcji.cpp : Defines the entry point for the console application.
//

/*
- tworzenie wskaznikow dla pojedynczych zmiennych jest czasem marnowaniem pamieci, dlatego preferuje sie 
tworzenie wskaznikow do struktur 
- propozycja nastepnej lekcji wskazniki do funkcji

- styl gwiazdki int* p - styl c++, int *p - styl c
*/

#include "stdafx.h"
#include <iostream>
#include <ctype.h> //prototypes for is lower, touppear

using namespace std;
void passByValue(int data);
void passByPointer(int *data);
void convertToUppercase(char *sPtr);
void copy1(char *s1, const char *s2);
void copy2(char *s1, const char *s2);
void connectTable(char* s, char* t);
char tekst1[30] = "Patrycja";
char tekst2[] = "Paduch";

int main()
{
	int apples = 10;
	int oranges = 20;
	static int a = 30;
	passByValue(apples);
	passByPointer(&oranges);
	cout << "Ilosc jablek: " << apples << endl;
	cout << "Ilosc pomaranczy: " << oranges << endl;
	cout << a << endl;
	
	char b;
	char *wsk = &b;
	char **wsk1 = &wsk; //wskaznik do wskaznika do char 
	int *tab[10]; //tablica 10 wskaznikow do int
	void *p; //wskaznik na obiekt nieznanego typu, gdy chcemy przekazac tylko info o adresie

	char phrase[] = "ala ma kota";
	cout << "Fraza przed zmiana: " << phrase << endl;
	convertToUppercase(phrase);
	cout << "Fraza po zmianie: " << phrase << endl;

	char string1[20];
	char *string2 = "Hello world";
	char string3[20];
	char string4[] = "C++ wooow";
	copy1(string1, string2);
	cout << "Po skopiowaniu string1: " << string1 << endl;
	copy2(string3, string4);
	cout << "Po skopiowaniu string3: " << string3 << endl;

	//char tekst1[30] = "Patrycja";
	//char tekst2[] = "Paduch";
	connectTable(tekst1, tekst2);
	cout << tekst1 << endl;

	getchar();
    return 0;
}

void passByValue(int data) {
	data = 100;
	int a = 20;
}

void passByPointer(int *data) {
	*data = 200;
}

//zamienia string na duze litery
void convertToUppercase(char *sPtr)
{
	while (*sPtr != '\0')
	{
		if(islower(*sPtr)) //trzeba zainkludowac <ctype.h>, zwraca != 0 gdy argument jest mala litera
			*sPtr = toupper(*sPtr); //zwraca znak zmieniony z malej litery na duza
		++sPtr; 
	}
}

//kopiuje string2 do string1
void copy1(char *s1, const char *s2)
{
	for (int i = 0; (s1[i] = s2[i]) != '\0'; i++)
		;
}

void copy2(char *s1, const char *s2)
{
	for (; (*s1 = *s2) != '\0'; s1++, s2++)
		;
}



/*
Napisa� funkcj� sklej, kt�rej parametrami s� dwa wska�niki na tekst s i t. Funkcja ma dopisa� do
tekstu znajduj�cego si� w tablicy s tekst t. Funkcja ma operowa� na wska�nikach do tekstu, a nie na
tablicach i indeksowaniu. Funkcja nie mo�e utworzy� trzeciego tekstu - ma doklei� drugi do
pierwszego. Uwaga, tablica zawieraj�ca tekst pierwszy musi by� wystarczaj�co du�a, by
pomie�ci�a dodatkowo znaki tekstu drugiego.
*/

void connectTable(char* s, char* t)
{
	while (*s != '\0')
	{
		s++;
	}
	*s = ' '; 
	s++;
	for (; (*s = *t) != '\0'; s++, t++)
		;
	
}
