// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <time.h>
#include <stdlib.h>
//wskaznik - to zmienna przechowujaca w sobie adres w pamieci RAM innej zmiennej 

//kazdy bajt w pamieci ma swoj numer, dlatego zmienna int przechowywana jest w 4 komorkach pamieci
//wskaznik jest przechowywany tez pod jakims adresem i przechowuje tam wartosc adresu na ktory wskazuje

/*
4 mozliwosci wskaznikow
- dynamiczne rezerwowanie i zwalnianie obszarow pamieci
- zwiekszenie szybkosci zapisu i odczytu komorek w tablicy
- uzywanie w funkcjach oryginalow zmiennych z programu wywolujacego
- mozliwosci wspolpracy z urzadzeniem zewnetrznym np. miernikiem temperatury

*/

//Zwiazek wskaznikow z tablica
//Nazwa tablicy jest adresem pierwszego elementu tablicy (jest wskaznikiem do pierwszego elementu) , to *tab wtedy to wartosc pierwszego elementu
//tab = &tab[0]
//*tab = tab[0]

using namespace std;
int main()
{
	clock_t start, stop;
	double time;
	//przypomnienie o tablicy statycznej
	int tablica[5] = {1, 2, 3, 4, 5};
	cout << *tablica << endl;  //wyswietli tablica[0]
	cout << tablica << endl; //wartosc wskaznika adres pierwszego elementu

	int size; 
	int house = 120;
	int *wsk; //wskaznik na typ int
	wsk = &house; //przypisanie adresu zmiennej house do wsk
	cout << "Adres zmiennej x: " << wsk << endl;
	cout << "Wartosc zmiennej x poprzez wskaznik: " << *wsk << endl; //operator dereferencji, pokazuje zwartosc tego na co wskazuje wsk

	int *y = nullptr; //y to pusty wskaznik, nullptr to typ wskaznikowy o wartosci 0. W C uzywany jest NULL on tez oznacza 0 ale nie jest typem wskaznikowym
					  //tylko liczbowym. Jest on uzywany do obiektow ktore nie maja swojego adresu np. koniec listy. 
					  //Nullptr wykorzystywany jest w funkcjach przeciazonych
	cout << sizeof(y) << endl;
	cout << NULL << endl;
	
	//tablica dynamiczna - pozwala na definiowanie i edycje rozmiaru w trakcie dzia�ania programu. Gdy nie jest juz potrzebna mozna ja skasowac z pamieci
	//czesto przydaje sie zwolnienie pamieci RAM, np. w grach przy przejsciu do kolejnego levelu pewne informacje juz nie sa potrzebne
	cout << "Podaj rozmiar tablicy: " << endl;
	cin >> size;
	int *table; //najpierw deklaracja wskaznika tego samego typu co elementy tablicy
	table = new int[size]; //zarezerwowanie miejsca w pamieci o okreslonym typie
	int i;

	start = clock();
	for (i = 0; i < size; i++)
	{
		table[i] = i;
		table[i] += 100;
	}

	stop = clock();

	time = (double)(stop - start) / CLOCKS_PER_SEC; //rzutowanie obydwoch typow na double bo sa clock_t
	//dzielac ten czas przez ta zmienna otrzymujemy czas
	cout << "Czaz bez wskaznikow" << time << endl;

	

	delete[] table;

	//porownaj czas w przypadku
	//Wypisywanie adresow kolejnych elementow tablicy
	int *table1;
	table1 = new int[size];
	start = clock();
	for (i = 0; i < size; i++)
	{
		*table1 = i;
		*table1 += 100;
		table++; //przejscie na kolejny adres tablicy, do kolejnego elementu
	}
	stop = clock();
	time = (double)(stop - start) / CLOCKS_PER_SEC; 
	cout << "Czaz ze wskaznikiem" << time << endl;

//	for (i = 0; i < size; i++)
//	{
//		cout << *table << endl;
//		table++;
//	}


	delete [] table1;

	getchar();
	getchar();
    return 0;
}

void funkcja(char *x, char *y)
{
	if (x == nullptr) //sprawdzenie czy argument wskaznikowy rzeczywiscie na cos wskazuje
	{
		//reakcja na blad
	}
}
