#include <iostream>

//wartosci ktore przypisujemy do zmiennej moga byc w formie dec np. 20, w formie hex 0x14, 0b10100, osemkowo 055


using namespace std;

int main()
{
    //operatory arytmetyczne +, -, *, /, % reszta z dzielenia (operator modulo)
    // 1. cwiczenie podaj zmienne a, b typu int, wykonaj na nich operacje

    // 2. cwiczenie: podaj zmienne c,d typu float, wykonaj na nich operacje


    //operatory logiczne:
    // operator lub (suma logiczna)|| - zwraca prawde gdy przynajmniej jeden z  warunkow jest prawdziwy
    int i=10; int j=15; int k=-2; int m=200;
    bool zmienna1 = i<j || j==k;
    bool zmienna2 = i>k || m<j;

    //operator i -  koniunkcji (iloczyn logiczny) - && - zwraca prawde gdy obdywa przypadki sa prawdziwe
    bool zmienna3 = i != m && j<m;
    bool zmienna4 = k<j && j<i;

    //Operatory relacyjne:
    // < , >,  <=, >=, ==, != (rozny)

    //operatory przypisania:
    int zmienna5 = 10;


    //inkrementacja, dekrementacja
    int zmienna5 = 1;
    cout << zmienna5++ << endl; //operator postinkrementacji
    cout << zmienna5++ << endl; //operator preinkrementacji
    cout << ++zmienna5 << endl;
    zmienna5 = zmienna5 + 1;
    cout << zmienna5 << endl;
    zmienna5 += 1;
    cout << zmienna5-- << endl;
    cout << --zmienna5 << endl;
    zmienna5 = zmienna5 - 1;
    zmienna5 -= 1;


    // Operator negacji
    int i = 0;
    bool gotowe;
    if(!i)
    {
        cout << "Uwaga bo i jest rowne zero \n";
    }
    gotowe = true;
    if(!gotowe)
    {
        cout << "Jestes gotowy do wakacji \n;
    }
    else
    {
        cout << "Uwaga nie jestes gotowy do wakacji \n";
    }



    //Zastosowanie petla if
    int zmienna6 = 2;
    if (zmienna5 == zmienna6)
    {
        zmienna5++;
    }




    cout << "Hello world!" << endl;
    return 0;
}


//Kiedy wybrac jaki typ zmiennych:
//najczesciej int, jezeli bedzie przypisywac do zmiennej liczby dodatnie to unsigned
//jezeli wieksze niz 16 bitow to trzeba korzystac z long
//short uzywamy kiedy np.chcemy oszczedzac pamiec strukturze

// zadania
/*

//zad 2 -> sprawdzasz czy liczba jest parzysta oraz czy jest mniejsza od 10
	//zad 4 -> podana liczba jest nieparzysta lub (wieksza od 10 i jednoczesnie mniejsza od 20)

//	Napisz program, ktory pobiera od uzytkownika dwie liczby int, oblicza ich roznice, a nastepnie sprawdza, czy ta roznica
//przy dzieleniu przez 5 daje reszte 2

1. Napisze program ktory wczyta 4 liczby rzeczywiste z klawiatury a nastepnei wyswietli liczby >=0
i jednoczesnie < 10

2. Napisz program ktory wczyta 2 liczby calkowite i sprawdzi ich parzystosc

3. Napisz program ktory pobiera liczbe n, oraz 2 liczby calkowite i sprawdza czy sa one podzielne przez n

*/

//Zadanie operatory2
/*

Poslugujac sie operatorem modulo napisz program ktory wypisywal bedzie na ekranie kolejne liczby calkowite
poczawszy od 1 do 100 a po zakoczeniu kazdej dziesiatki (10, 20, 30, ...) wypisze tekst zakoczona dziesiatka

Uzywajac operatora reszta z dzielenia napisz program ktory bedzie wypisywal na ekranie kolejne litery alfabetu

mamy deklaracje int a = 4;
cout << (a=7) << endl; co wypisze sie na ekran
*/


