#include <stdio.h>

using namespace std;

/*
instrukcja break - ma zastosowanie w petli while, do while, switch, for
jezeli program na nia natrafi przerwie petle w ktorej znajdowala sie ta instrukcja.
Kolejna instrukcja wykonywana przez program to ta ktora znajduje sie bezposrednio za wykonywana petla

instrukcja continue - wykorzystywana wewnatrz for, while, do while. Powoduje zaniechanie instrukcji
wykonywanych wewnatrz petli w ktorej zostala wywolana, i przejscie do kolejnego obiegu (sprawdzenie warunku).
Nie przerywa zatem petli a tylko dany obieg
*/

int main()
{
    int mark, i, j;
    int k = 3;
	int licznik=0;

	while(licznik < 5)
	{
		printf("Petla while: %d",licznik++);
	}

	do
	{
		printf("Petla do while: %d", licznik++);
	}
	while(licznik<5);

	while(licznik<5)
    {
        printf("Petla while2: %d", licznik++);
    }

    //napiszmy petle nieskonczona ktora pobiera Twoje oceny, jezeli ocena sie nie zgadza <0, >6. Piszemy Klamiesz, koniec podawania
    while(1)
    {
      //  cout << "Podaj ocene. Koniec (0)" << endl;
  //      cin >> mark;
        if (mark == 0)
        {
            break;
      //      cout << "Ciekawe czy to sie wykona" << endl;
        }
      //  cout << "***" << endl;
    }
   // cout << "Jestem tutaj" << endl;

    for(i=0; i<5; i++)
    {
        for(j=0;j<10; j++)
        {
      //      cout << "*" ;
            if (j > k) break;
        }
      //  cout << "Kontynuujemy zewnetrzna petla a nie wychodzimy z calosci" << endl;
    }

    for(k=0; k<10; k++)
    {
   //     cout << "A";
        if(k>1) continue;
    //    cout << "b \n";
    }


 //   cout << "Piszemy" << endl;
    goto k;
 //   cout << "To sie pewnie nie wypisze" << endl;

    k:


    return 0;
}

////3. Ktore z trzech petli jakie poznales moga sprawic, ze zawarte w niej instrukcje
//nie zostana wcale wykonane. Napisz przyklad kodu (dla kazdej petli ktora podasz) z uyciem
//takich petli, tak aby wgl program nie wszedl do srodku petli
