#include <iostream>

//struktura funkcja
//typ_zwracany nazwa_funkcji(typ argument, typ argument, ...)
//{
// cialo funkcji, czyli instrukcje ktore funkcja wykonuje
//}

//funkcja ktora nic nie zwraca i nic nie przyjmuje nie mozna zatem nic
// do tej funkcji przekazac z zewnatrz, ona bedzie zawsze dzialac tak samo

//Napisz funkcje ktora przyjmuje 2 liczby calkowite i zwraca wieksza
//w funkcji main stworz zmienna przechowujaca wieksza liczbe i przypisz tej liczbie funkcje
//ktora napiszesz. Przyklad: wieksza=funkcja(a,b);

using namespace std;
int ktoraWieksza(int a, int b);

int main()
{
    int wieksza;
    wieksza = ktoraWieksza(2,8);
    cout << wieksza<< endl;
    return 0;
}

int ktoraWieksza(int a, int b) //na ta chwile umiemy pisac funkcje ktora potrafia zwrocic naraz tylko jedna wartosc jednego rodzaju
{
	if (a > b)
	{
		return a; //kiedy piszemy ze funkcja ma zwracac to znaczy ze musimy w jej ciele wywolac specjalna instrukcje return po ktorej napiszemy
		//co nasza funkcja ma zwracac
		//w momencie kiedy twoja funkcja napotka slowo return to natychmiast po tym jak zwroci konczy swoje dzialanie
	}
	else
	{
		return b; //w przypadku kiedy b jest wieksze lub rowne to zwraca b
	}
}
