// 11_tablice_wielowym_przekazywanie.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
//Najczesciej stosowana tablica wielowymiarowa to tablica dwuwymiarowa
int sumArray(const int a[][2], int); //w definicji funkcji c++ wymaga podania ilosci kolumn w wierszach
using namespace std;


int main()
{
	int tab[3][100]; //taka tablica to inaczej macierz ktora ma 3 wiersze i 2 kolumny. Tablica dwuwymiarowa 
	//przechowywana jest w pamieci wierszami, czyli tab[0][0], tab[0][1] tab[1][0] itd.
	tab[2][10] = 2; //ten element polozony o i * 100 + j = 210 miejsc dalej od pierwszego elementu
	int tab1[2][3] = { {2,5,3},{10,4,5} };
	int tab2[][2] = { 
		{1,2}, 
		{4,3}, 
		{2, 2} 
	}; //c++ wymaga podania ilosci kolumn w danym wierszu
	int tab3[3][2];
	int sum;
	cout << "Sum of elements in tab2 = " << sumArray(tab2, 3)<<  endl;
	
	getchar();
    return 0;
}



int sumArray(const int a[][2], int rowSize)
{
	int sum = 0;
	for (int row=0; row < rowSize; row++)
	{
		for (int column = 0; column < 2; column++)
		{
			sum += a[row][column];
		}
	}

	return sum;
}


//napisz funkcje ktora bedzie zajmowala sie wpisywaniem wartosci do tablicy dwuelementowej