// struktury.cpp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
//Struktury - jest to typ zlozony, przechowuje zmiennego roznego typu (rowniez funkcje), najczesciej sa powiazne ze soba dane

//struktury mozna przekazywac do funkcji, jezeli przez wartosc to przekazywana jest kopia, jezeli przez wskaznik to kopiowany jest oryginal

using namespace std; 

struct osoba //zmienne struktury sa domyslnie publiczne
{
	int wiek;
	string miejscowosc;
	string nazwisko;
	float IQ;
}Artur, Janek, Michal; //obiekty struktury, sposob1

struct samochod
{
	int rocznik;
	string kolor;
	int predkosc;
}proshe, audi{2016, "czarny", 120000};

struct BokiTrojkata
{
	double bok1;
	double bok2;
	double bok3;
};

struct Prostokat
{
	int bok1;
	int bok2;
};

bool sprawdzanieTrojkata(BokiTrojkata trojkat);
int poleProstokata(Prostokat prostokat1);

int main()
{
	bool check;
	BokiTrojkata trojkat1;
	cout << "Podaj pierwszy bok" << endl;
	cin >> trojkat1.bok1;
	cout << "Podaj drugi bok" << endl;
	cin >> trojkat1.bok2;
	cout << "Podaj trzeci bok" << endl;
	cin >> trojkat1.bok3;
	check = sprawdzanieTrojkata(trojkat1);
	cout << "Wynik sprawdzania trojkata: " <<check << endl;
	osoba Ania; //stworzenie dodatkowego obiektu struktury osoba, w przeciwienstwie do c, w c++ nie musimy pisac struct 
	Artur.wiek = 13; //inicjalizacja zmiennych
	Ania.wiek = 10; 
	osoba Janek = {18, "Katowice", "Nowak", 152};
	Prostokat prostokacik = {2, 7};
	int wynikPola;
	cout << "Pole prostokata" << poleProstokata(prostokacik) << endl;
	

	getchar();
	getchar();
    return 0;
}

//Zadanie 1
//Zdefiniuj strukture prostokat przechowujaca dlugosc bokow a i b. Napisz funkcje ktora otrzymuje
//jako argument zmienna typu struct prostokat i zwraca jako wartosc pole prostokata przekazanego w argumencie
int poleProstokata(Prostokat prostokat1)
{
	return (prostokat1.bok1*prostokat1.bok2);
}

//Zadanie 2
//stworz strukture trojkat, zainicjalizuj ja i przekaz do funkcji. Nastepnie w srodku funkcji sprawdz
//czy z takich bokow mozna stworzyc trojkat
//dowolny bok trojkata ma mniejsza dlugosc od sumy dlugosci pozostalych bokow 
bool sprawdzanieTrojkata(BokiTrojkata trojkat)
{
	if ((trojkat.bok1 < trojkat.bok2 + trojkat.bok3) && (trojkat.bok2 < trojkat.bok1 + trojkat.bok3) && (trojkat.bok3 < trojkat.bok1 + trojkat.bok2))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

//teraz ponizej piszemy funkcje, ktora pobiera jako argument strukture
void daneOsoby(/*struct osoba*/osoba x)
{
	printf("%s %s, %d, %f\n", x.imie, x.nazwisko, x.wiek, x.waga);
}